using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class PlayerInputManager : MonoBehaviour
{

    public void SetPlayerName(UnityEngine.UI.Text name_TXT)
    {
        string playerName = name_TXT.text;
        if (string.IsNullOrEmpty(playerName))
        {
            Debug.LogWarning("Player Name is empty");
            return;
        }

        PhotonNetwork.NickName = playerName;
    }
}
