using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject Player_Prefab;

    public static GameManager instance;

    private void Awake()
    {
        if (instance != null) Destroy(this.gameObject);
        else instance = this;
    }

    void Start()
    {
        if (!PhotonNetwork.IsConnected) return;
        if(Player_Prefab == null) return;

        int xRandPoint = Random.Range(-20, 20);
        int yRandPoint = Random.Range(-20, 20);

        PhotonNetwork.Instantiate(Player_Prefab.name, 
            new Vector3(xRandPoint, 0, yRandPoint), 
            Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnJoinedRoom()
    {
        Debug.Log($"{PhotonNetwork.NickName} has joined the room!");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log($"{PhotonNetwork.NickName} has entered the room {PhotonNetwork.CurrentRoom.Name}");
        Debug.Log($"Room has now {PhotonNetwork.CurrentRoom.PlayerCount}/20 players");
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("GameLauncher");
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }
}
