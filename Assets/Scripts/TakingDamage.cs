using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class TakingDamage : MonoBehaviourPunCallbacks
{
    private float Start_HP = 100;

    private float current_HP;

    [SerializeField] private UnityEngine.UI.Image hp_bar;
    // Start is called before the first frame update
    void Start()
    {
        current_HP = Start_HP;
        hp_bar.fillAmount = current_HP / Start_HP;
    }

    [PunRPC]
    public void TakeDamage(float dmg)
    {
        current_HP -= dmg;
        
        Debug.Log(current_HP);
        
        hp_bar.fillAmount = current_HP / Start_HP;
        
        if (current_HP <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        if(!photonView.IsMine) return;
        
        GameManager.instance.LeaveRoom();
    }
}
