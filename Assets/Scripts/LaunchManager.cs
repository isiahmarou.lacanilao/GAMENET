using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Random = UnityEngine.Random;

public class LaunchManager : MonoBehaviourPunCallbacks
{
    public GameObject EnterGame_Panel, Connecting_Panel, Lobby_Panel;

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Start()
    {
        EnterGame_Panel.SetActive(true);
        Connecting_Panel.SetActive(false);
        Lobby_Panel.SetActive(false);
    }
    

    public override void OnConnected()
    {
        Debug.Log($"{PhotonNetwork.NickName} Connected To Internet");
    }
    
    public override void OnConnectedToMaster()
    {
        Debug.Log($"{PhotonNetwork.NickName} Connected To Master Server");
        EnterGame_Panel.SetActive(false);
        Connecting_Panel.SetActive(false);
        Lobby_Panel.SetActive(true);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log(message);
        CreateAndJoinRoom();
    }

    public void ConnectToPhotonServer()
    {
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
            EnterGame_Panel.SetActive(false);
            Connecting_Panel.SetActive(true);
            Lobby_Panel.SetActive(false);
        }
    }

    public void JoinRandomRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    private void CreateAndJoinRoom()
    {
        string randRoomName = $"Room {Random.Range(0, 10000)}";

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsOpen = roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 20;

        PhotonNetwork.CreateRoom(randRoomName, roomOptions);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log($"{PhotonNetwork.NickName} has entered {PhotonNetwork.CurrentRoom.Name}");
        PhotonNetwork.LoadLevel("GameScene");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log($"{newPlayer.NickName} has entered room {PhotonNetwork.CurrentRoom.Name}. " +
                  $"Room has {PhotonNetwork.CurrentRoom.PlayerCount} players");
    }
}
