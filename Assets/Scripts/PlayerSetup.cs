using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject Camera;
    [SerializeField] private TMP_Text playerName_TXT;
    
    void Start()
    {
        transform.GetComponent<MovementController>().enabled = photonView.IsMine;
        Camera.GetComponent<Camera>().enabled = photonView.IsMine;

        playerName_TXT.text = photonView.Owner.NickName;
    }
}
